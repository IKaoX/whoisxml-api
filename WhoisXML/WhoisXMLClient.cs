﻿using System;
using System.Threading.Tasks;
using WhoisXML.Client;
using WhoisXML.Client.Abstraction;
using WhoisXML.Model.Response;

namespace WhoisXML
{
    public class WhoisXMLClient : IWhoisXMLClient
    {
        
        public WhoisXMLClient(string token)
        {
            User = new UserClient(token);
            ReverseNS = new ReverseNSClient(token);
        }

        /// <summary>
        /// 
        /// </summary>
        public IUserClient User { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public IReverseNSClient ReverseNS { get; set; }
    }
}