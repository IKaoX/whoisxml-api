﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WhoisXML.Http
{
    public static class HttpRequest<Response> where Response : class
    {
        public static Task<Response> SendRequest(string request)
        {
            var webRequest = WebRequest.Create(request);
            webRequest.ContentType = "application/json";

            var webResponse = webRequest.GetResponse();
            var httpResponse = (HttpWebResponse) webResponse;
            
            if (httpResponse.StatusCode != HttpStatusCode.OK)
                throw new HttpRequestException();

            using var responseStream = webResponse.GetResponseStream();

            if (responseStream == null)
                throw new NullReferenceException();

            using var readResponse = new StreamReader(responseStream);
            var jsonResponse = readResponse.ReadToEnd();
            var response = JsonConvert.DeserializeObject<Response>(jsonResponse);

            if (response == null)
                throw new NullReferenceException();
            
            return Task.FromResult(response);
        }
    }
}