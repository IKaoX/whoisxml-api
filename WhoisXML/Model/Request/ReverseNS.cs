﻿namespace WhoisXML.Model.Request
{
    public class ReverseNS
    {
        public ReverseNS(string nameServer)
        {
            NameServer = nameServer;
        }
        
        public ReverseNS(string nameServer, string from)
        {
            NameServer = nameServer;
            From = from;
        }
        
        public string NameServer { get; set; }
        public string From { get; set; }
    }
}