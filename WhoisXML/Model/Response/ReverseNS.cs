﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace WhoisXML.Model.Response
{
    public class ReverseNS
    {
        [JsonProperty("current_page")]
        public string CurrentPage { get; set; }
        
        [JsonProperty("size")]
        public string PageSize { get; set; }
        
        [JsonProperty("result")]
        public IReadOnlyList<Domain> Domains;
        
        public class Domain
        {
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("name")]
            public string Hostname { get; set; }
            
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("first_seen")]
            public long FirstSeen { get; set; }
            
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("last_visit")]
            public long LastVisit { get; set; }
        }
    }
}