﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace WhoisXML.Model.Response
{
    public class Balance
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("data")]
        public IReadOnlyList<BalanceData> Data { get; set; }

        public int DomainResearchSuiteBalance => Data.FirstOrDefault(x => x.Id == 14)?.Credits ?? 0;

        /// <summary>
        /// 
        /// </summary>
        public class Product
        {
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("id")]
            public int Id { get; set; }
            
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("name")]
            public string Name { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        public class BalanceData
        {
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("product_id")]
            public int Id { get; set; }
            
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("product")]
            public Product Product { get; set; }
            
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("credits")]
            public int Credits { get; set; }
        }
    }
}