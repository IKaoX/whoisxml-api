﻿using System;
using System.Threading.Tasks;
using WhoisXML.Client.Abstraction;
using WhoisXML.Http;
using WhoisXML.Model.Response;

namespace WhoisXML.Client
{
    public class ReverseNSClient : IReverseNSClient
    {
        private readonly string _requestUrl = "https://reverse-ns.whoisxmlapi.com/api/v1?apiKey=";

        public ReverseNSClient(string token)
        {
            _requestUrl = $"{_requestUrl}{token}";
        }
        
        public async Task<ReverseNS> Get(Model.Request.ReverseNS request)
        {
            var response = await HttpRequest<ReverseNS>
                .SendRequest($"{_requestUrl}&ns={request.NameServer}&from={request.From}");

            if (response == null)
                throw new NullReferenceException();

            return response;
        }
    }
}