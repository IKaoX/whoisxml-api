﻿using System;
using System.Threading.Tasks;
using WhoisXML.Client.Abstraction;
using WhoisXML.Http;
using WhoisXML.Model.Response;

namespace WhoisXML.Client
{
    public class UserClient : IUserClient
    {
        private readonly string _balanceRequestUrl = "https://user.whoisxmlapi.com/service/account-balance?apiKey=";

        public UserClient(string token)
        {
            _balanceRequestUrl = $"{_balanceRequestUrl}{token}";
        }
        
        public async Task<Balance> GetBalance()
        {
            var response = await HttpRequest<Balance>
                    .SendRequest($"{_balanceRequestUrl}");

            if (response == null)
                throw new NullReferenceException();

            return response;
        }
    }
}