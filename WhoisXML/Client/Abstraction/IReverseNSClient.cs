﻿using System.Threading.Tasks;
using WhoisXML.Model;

namespace WhoisXML.Client.Abstraction
{
    public interface IReverseNSClient
    {
        public Task<Model.Response.ReverseNS> Get(Model.Request.ReverseNS request);
    }
}