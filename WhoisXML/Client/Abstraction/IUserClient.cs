﻿using System.Threading.Tasks;
using WhoisXML.Model.Response;

namespace WhoisXML.Client.Abstraction
{
    public interface IUserClient
    {
        public Task<Balance> GetBalance();
    }
}