﻿using System.Threading.Tasks;
using WhoisXML.Client.Abstraction;
using WhoisXML.Model.Response;

namespace WhoisXML
{
    public interface IWhoisXMLClient 
    {
        public IUserClient User { get; set; }
        public IReverseNSClient ReverseNS { get; set; }
    }
}